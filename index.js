const express = require("express");
const webpush = require("web-push");
const bodyParser = require("body-parser");

const app = express();

// Set static path
app.use(express.static("public"));

app.use(bodyParser.json());

const publicVapidKey = "BKysZEpGSwhec_jEz5Co4LhlW7DnN6x1FzGdlvGRw9gJr54ZQtckHSkVMwdYgK0jMaencTqp-OJ54a1Wt1HFUkQ";
const privateVapidKey = "CqmC6agcGkJhKAK-_1n0IxdN2qJhXKASg4XNRMWX31Q";

webpush.setVapidDetails(
  "mailto:test@test.com",
  publicVapidKey,
  privateVapidKey
);

//to store multiple subs (temporary)
let sublist = [];

function manageSubs(subscription) {
  console.log('inside manage subs');
  let found = false;

  if (subscription.endpoint == undefined) return;
  //check if already in list
  for (var i = 0; i < sublist.length; i++) {
    if (sublist[i].endpoint == subscription.endpoint) {
      found = true;
      console.log('found sub...skipping');
      break;
    }
  }
  //if not, add sub
  if (!found) {
    console.log('adding sub...');
    sublist.push(subscription);
    console.log(sublist);
  }
}

// Subscribe Route
app.post("/subscribe", (req, res) => {
  // Get pushSubscription object
  const subscription = req.body;

  //add sub to temporary array
  manageSubs(subscription)
  // Send 201 - resource created
  res.status(201).json({});

  // Create payload
  const payload = JSON.stringify({
    title: "Push Test",
    body: "hey there, from Vic Api",
    icon: "https://cdn.pixabay.com/photo/2020/04/03/06/35/work-4997565_960_720.png"
  });

  // Pass object into sendNotification
  sublist.forEach(sub => {
    webpush.sendNotification(sub, payload).catch(err => console.error(err));
  })
});

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));
