const publicVapidKey =
  "BKysZEpGSwhec_jEz5Co4LhlW7DnN6x1FzGdlvGRw9gJr54ZQtckHSkVMwdYgK0jMaencTqp-OJ54a1Wt1HFUkQ";

let subscription;

// Check for service worker
if ("serviceWorker" in navigator) {
  setup().catch(err => console.error(err));
}

// Register SW, Register Push
async function setup() {
  // Register Service Worker
  console.log("Registering service worker...");
  const register = await navigator.serviceWorker.register("/worker.js", {
    scope: "/"
  });
  console.log("Service Worker Registered...");

  await setTimeout(() => registerPush(register), 2000);
  // Can now send Push Notifications
}

async function registerPush(register) {
  // Register Push
  console.log("Registering Push...");
  subscription = await register.pushManager.subscribe({
    userVisibleOnly: true,
    applicationServerKey: urlBase64ToUint8Array(publicVapidKey)
  });
  console.log("Push Registered...");
}

function urlBase64ToUint8Array(base64String) {
  const padding = "=".repeat((4 - base64String.length % 4) % 4);
  const base64 = (base64String + padding)
    .replace(/\-/g, "+")
    .replace(/_/g, "/");

  const rawData = window.atob(base64);
  const outputArray = new Uint8Array(rawData.length);

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}

async function sendNotification() {
  console.log("Sending Push...");
  await fetch("/subscribe", {
    method: "POST",
    body: JSON.stringify(subscription),
    headers: {
      "content-type": "application/json"
    }
  });
  console.log("Push Sent...");
}
